<?php

namespace App\Entity;

class Furniture{

  private $id;
  private $name;
  private $price;
  private $type;
  private $color;
  private $ingPath;
  
  public function __construct(string $name, float $price, string $type, string $color, string $ingPath, int $id) {
      $this->id =$id;
      $this->name =$name;
      $this->price=$price;
      $this->type=$type;
      $this->color=$color;
      $this->ingPath=$ingPath;
  }
  public function getName(): string
  {
      return $this->name;
  }

      public function setName(string $name): void
  {
      $this->name = $name;
  }

  public function getPrice(): float
  {
      return $this->price;
  }

      public function setPrice(Float $price): void
  {
      $this->price = $price;
  }

  public function getType(): string
  {
      return $this->type;
  }

     public function setType(string $type)
  {
      $this->type = $type;
  }


  public function getColor(): string
  {
      return $this->color;
  }

     public function setColor(string $color): void
  {
      $this->color = $color;
  }

  public function getIngPath(): string
  {
      return $this->ingPath;
  }

     public function setIngPath(string $ingPath): void
  {
      $this->ingPath = $ingPath;
  }

  public function getId(): int
  {
      return $this->id;
  }

  public function setId(int $id): void
  {
      $this->id = $id;
  }

}