<?php


namespace App\Controller;

use App\Repository\FurnitureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FurnitureController extends AbstractController{


    /**
     * @Route ("/furniture", name="furniture")
     */
    public function Furniture(){

   $repository = new FurnitureRepository();
   $reserve = $repository->findAll();
 

   return $this->render('furniture.html.twig',[

        'reserve' => $reserve
        ]);
    }
}