CREATE DATABASE simplonKea;

USE simplonKea;

CREATE TABLE Furniture (id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, name VARCHAR(20), price FLOAT, type VARCHAR(30), color VARCHAR(20), ingPath VARCHAR(64));

INSERT INTO Furniture (name, price, type, color, ingPath) VALUE ('table', '342', 'modern', 'white', 'https://www.zespoke.com/fr/product/retro-cassettes-coffee-table-large-hoop/?gclid=EAIaIQobChMI7cnm4tP_5AIVx7HtCh2L-QNsEAQYAyABEgLG9PD_BwE');
INSERT INTO Furniture (name, price, type, color, ingPath) VALUE ('sofa', '350', 'vintage', 'green', 'https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcQOmOezwU0tS3BM7TxXckfj6tCCNFgNGjujklB8tt0iKTgi12I9VmZDlEdieT8OEZ0HqgCC2F-3KUFN9c41xRULrCJmlomVC1UcobWzdCYGPSKyINt7BTyDAA&usqp=CAc');
INSERT INTO Furniture (name, price, type, color, ingPath) VALUE ('chair', '150', 'modern', 'green', '');
INSERT INTO Furniture (name, price, type, color, ingPath) VALUE ('living room table', '120,50', 'design','white', '');
INSERT INTO Furniture (name, price, type, color, ingPath) VALUE ('living room table', '120,50', 'design','white', '');
INSERT INTO Furniture (name, price, type, color, ingPath) VALUE ('bar stool', '300', 'modern', 'black', '');

UPDATE simplonKea SET ingPath='https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcQOmOezwU0tS3BM7TxXckfj6tCCNFgNGjujklB8tt0iKTgi12I9VmZDlEdieT8OEZ0HqgCC2F-3KUFN9c41xRULrCJmlomVC1UcobWzdCYGPSKyINt7BTyDAA&usqp=CAc') WHERE id =1;