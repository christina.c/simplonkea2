<?php

namespace App\Repository;

use App\Entity\Furniture;

class FurnitureRepository
{
    private $pdo;

    public function __construct() {

        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'] .';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV ['DATABASE_PASSWORD']

        );
    }
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM Furniture');
    
        $query->execute();

        $results = $query->fetchAll();
        $list = [];
   
        foreach ($results as $line) {
  
            $furniture = $this->sqlToFurniture($line);

            $list[] = $furniture;
        }
   
        return $list;
    }

    public function add(Furiniture $furniture): void {
 
        $query = $this->pdo->prepare('INSERT INTO Furniture (name, price, type, color, ingPath) VALUES (:name,:price,:type,:color,:ingPath)');
  
        $query->bindValue('name', $furniture->getName(), \PDO::PARAM_STR);
        $query->bindValue('price', $furniture->getPrice(), \PDO::PARAM_STR);
        $query->bindValue('type', $furniture->getType(), \PDO::PARAM_STR);
        $query->bindValue('color', $furniture->getColor(), \PDO::PARAM_STR);
        $query->bindValue('ingPath', $furniture->getIngPath(), \PDO::PARAM_INT);
         $query->bindValue('id', $furniture->getId(), \PDO::PARAM_INT);
       
        $query->execute();

        $furniture->setId(intval($this->pdo->lastInsertId()));
    }


    public function findById(int $id): ?Furniture {

        $query = $this->pdo->prepare('SELECT * FROM Furniture WHERE id=:idPlaceholder');

        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);

        $query->execute();
        $line = $query->fetch();
        if($line) {
            return $this->sqlToFurniture($line);
        }
    
        return null;

    }

    private function sqlToFurniture(array $line):Furniture {
        return new Furniture($line['name'], $line['price'], $line['type'], $line['color'], $line ['ingPath'],$line['id']);
    }

    public function delete(int $id){
        
        $delete = $this->pdo->prepare('DELETE FROM Furniture WHERE id=:idPlaceholder');
        $delete -> bindValue('/idPlaceHolder', $id, \PDO::PARAM-INT);
        $delete->execute();
    }
    
    public function update(int $id){

        $update = $this->pdo->prepare('UPDATE Furniture SET VALUE=? WHERE id=:idPlaceholder');
        $update -> bindValue('/idPlacholder', $id, \PDO::PRAM_INT);
        $update -> execute();
    }
}

